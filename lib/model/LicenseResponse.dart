import 'package:flutter/material.dart';

class LicenseResponse {
  String license_key = "";
  String expiry_date = "";
  String message = "";


  LicenseResponse(this.license_key, this.expiry_date, this.message);

  LicenseResponse.fromJson(Map<String, dynamic> json){
    license_key = json['license_key'];
    expiry_date = json['expiry_date'];
    message = json['message'];
  }
}