class Store {
  String No = "";
  String Name = "";
  String Functionality_Profile = "";

  Store({this.No, this.Name, this.Functionality_Profile});

  factory Store.fromJson(Map<String, dynamic> json) {
    return Store(
      No: json['No'],
      Name: json['Name'],
      Functionality_Profile: json['Functionality_Profile'],
    );
  }
}
