class PosFunctionalityProfile{
  final String POS_Currency_Symbol;

  PosFunctionalityProfile({this.POS_Currency_Symbol});

  factory PosFunctionalityProfile.fromJson(Map<String, dynamic> json) {
    return PosFunctionalityProfile(
      POS_Currency_Symbol: json['POS_Currency_Symbol'],

    );
  }

}