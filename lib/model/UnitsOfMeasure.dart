class UnitsOfMeasure {

   int id;
   String code;
   String description;
   String pos_min_denominator;
   bool weight_unit_of_measure = false;
   String international_standard_code;
   bool max_used = false;

   UnitsOfMeasure({this.id, this.code, this.description,
       this.pos_min_denominator, this.weight_unit_of_measure,
       this.international_standard_code, this.max_used});


   factory UnitsOfMeasure.fromJson(Map<String, dynamic> json) {
     return UnitsOfMeasure(
       code: json['Code'],
       description: json['Description'],
       pos_min_denominator: json['POS_Min_Denominator'],
       weight_unit_of_measure: json['Weight_Unit_Of_Measure'],
       international_standard_code: json['International_Standard_Code'],
       max_used: json['Max_Used'],
     );
   }
}