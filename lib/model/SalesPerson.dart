import 'package:floor/floor.dart';

@entity
class SalesPerson {
  @primaryKey
  String id;

  String first_name;

  String last_name;

  SalesPerson(this.id, this.first_name, this.last_name);

  SalesPerson.fromJson(Map<String, dynamic> json){
    id = json['ID'];
    first_name = json['First_Name'];
    last_name = json['Last_Name'];
  }




}