import 'package:floor/floor.dart';

@entity
class UOM{

  @primaryKey
  int id;

  String code;

  String description;

  UOM(this.id, this.code, this.description);


}