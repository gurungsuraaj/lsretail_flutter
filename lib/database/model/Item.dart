class Item{

  String description;
  String unitPrice;

  Item({this.description, this.unitPrice});

  factory Item.fromJson(Map<String, dynamic> json){
    return Item(
    description: json['Description'],
    unitPrice: json['Unit_Cost'],
    );
  }

}