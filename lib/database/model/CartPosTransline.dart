import 'package:floor/floor.dart';

@entity
class CartPosTransline {

  @PrimaryKey(autoGenerate: true)
  int item_key;

  String receipt_no;
  String item_no;
  String item_no2;
  String description;
  double unit_price;
  double discount_amount;
  double vat_amount;
  double amount;
  int qty;
  double net_amt;
  double calculated_amt;
  int entry_type;


  CartPosTransline({this.item_key, this.receipt_no, this.item_no, this.item_no2,
      this.description, this.unit_price, this.discount_amount, this.vat_amount,
      this.amount, this.qty, this.net_amt, this.calculated_amt,
      this.entry_type});

  factory CartPosTransline.fromJson(Map<String, dynamic> json){
    return CartPosTransline(
        item_no: json['No'],
        description: json['Description'],
        unit_price: json['Unit_Price'],
    );
  }


}