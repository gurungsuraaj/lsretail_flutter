// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AppDatabase.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String name;

  final List<Migration> _migrations = [];

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final database = _$AppDatabase();
    database.database = await database.open(name ?? ':memory:', _migrations);
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String> listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  UnitsOfMeasureDao _unitsOfMeasureDaoInstance;

  SalesPersonDao _salesPersonDaoInstance;

  Future<sqflite.Database> open(String name, List<Migration> migrations) async {
    final path = join(await sqflite.getDatabasesPath(), name);

    return sqflite.openDatabase(
      path,
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
      },
      onUpgrade: (database, startVersion, endVersion) async {
        MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);
      },
      onCreate: (database, _) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `UOM` (`id` INTEGER, `code` TEXT, `description` TEXT, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `SalesPerson` (`id` TEXT, `first_name` TEXT, `last_name` TEXT, PRIMARY KEY (`id`))');
      },
    );
  }

  @override
  UnitsOfMeasureDao get unitsOfMeasureDao {
    return _unitsOfMeasureDaoInstance ??=
        _$UnitsOfMeasureDao(database, changeListener);
  }

  @override
  SalesPersonDao get salesPersonDao {
    return _salesPersonDaoInstance ??=
        _$SalesPersonDao(database, changeListener);
  }
}

class _$UnitsOfMeasureDao extends UnitsOfMeasureDao {
  _$UnitsOfMeasureDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _uOMInsertionAdapter = InsertionAdapter(
            database,
            'UOM',
            (UOM item) => <String, dynamic>{
                  'id': item.id,
                  'code': item.code,
                  'description': item.description
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final _uOMMapper = (Map<String, dynamic> row) => UOM(
      row['id'] as int, row['code'] as String, row['description'] as String);

  final InsertionAdapter<UOM> _uOMInsertionAdapter;

  @override
  Future<List<UOM>> getAll() async {
    return _queryAdapter.queryList('SELECT * FROM UOM', mapper: _uOMMapper);
  }

  @override
  Future<void> deleteAll() async {
    await _queryAdapter.queryNoReturn('DELETE FROM UOM');
  }

  @override
  Future<void> insertAll(List<UOM> unitsOfMeasures) async {
    await _uOMInsertionAdapter.insertList(
        unitsOfMeasures, sqflite.ConflictAlgorithm.replace);
  }
}

class _$SalesPersonDao extends SalesPersonDao {
  _$SalesPersonDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _salesPersonInsertionAdapter = InsertionAdapter(
            database,
            'SalesPerson',
            (SalesPerson item) => <String, dynamic>{
                  'id': item.id,
                  'first_name': item.first_name,
                  'last_name': item.last_name
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final _salesPersonMapper = (Map<String, dynamic> row) => SalesPerson(
      row['id'] as String,
      row['first_name'] as String,
      row['last_name'] as String);

  final InsertionAdapter<SalesPerson> _salesPersonInsertionAdapter;

  @override
  Future<List<SalesPerson>> getAll() async {
    return _queryAdapter.queryList('SELECT * FROM SalesPerson',
        mapper: _salesPersonMapper);
  }

  @override
  Future<void> deleteAll() async {
    await _queryAdapter.queryNoReturn('DELETE FROM SalesPerson');
  }

  @override
  Future<void> insertAll(List<SalesPerson> salesPerons) async {
    await _salesPersonInsertionAdapter.insertList(
        salesPerons, sqflite.ConflictAlgorithm.replace);
  }
}
