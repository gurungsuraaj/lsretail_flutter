import 'package:floor/floor.dart';
import 'package:ls_retail/database/dao/UnitsOfMeasureDao.dart';
import 'package:ls_retail/database/model/UOM.dart';
import 'package:ls_retail/model/SalesPerson.dart';
import 'package:ls_retail/model/UnitsOfMeasure.dart';
import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'package:ls_retail/database/dao/SalesPersonDao.dart';

part 'AppDatabase.g.dart';

@Database(version: 1, entities: [UOM, SalesPerson])
abstract class AppDatabase extends FloorDatabase{

  UnitsOfMeasureDao get unitsOfMeasureDao;

  SalesPersonDao get salesPersonDao;
}