import 'package:floor/floor.dart';
import 'package:ls_retail/database/model/CartPosTransline.dart';

@dao
abstract class CartPosTranslineDao{

  @insert
  Future<void> insertCarPosTransline(CartPosTransline cartPosTransline);

}