import 'package:floor/floor.dart';
import 'package:ls_retail/model/SalesPerson.dart';

@dao
abstract class SalesPersonDao{

  @Insert(onConflict: OnConflictStrategy.REPLACE)
  Future<void> insertAll(List<SalesPerson> salesPerons);

  @Query('SELECT * FROM SalesPerson')
  Future<List<SalesPerson>> getAll();

  @Query('DELETE FROM SalesPerson')
  Future<void> deleteAll();
}