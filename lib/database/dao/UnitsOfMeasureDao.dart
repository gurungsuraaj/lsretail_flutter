import 'package:floor/floor.dart';
import 'package:ls_retail/database/model/UOM.dart';

@dao
abstract class UnitsOfMeasureDao{

  @Insert(onConflict: OnConflictStrategy.REPLACE)
  Future<void> insertAll(List<UOM> unitsOfMeasures);

  @Query('SELECT * FROM UOM')
  Future<List<UOM>> getAll();

  @Query('DELETE FROM UOM')
  Future<void> deleteAll();

}