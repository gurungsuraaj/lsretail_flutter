// Flutter code sample for material.RaisedButton.1

// This sample shows how to render a disabled RaisedButton, an enabled RaisedButton
// and lastly a RaisedButton with gradient background.
//
// ![Three raised buttons, one enabled, another disabled, and the last one
// styled with a blue gradient background](https://flutter.github.io/assets-for-api-docs/assets/material/raised_button.png)

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ls_retail/model/SalesPerson.dart';
import 'package:ls_retail/screens/configure.dart';
import 'package:ls_retail/utils/CompareDate.dart';
import 'package:ls_retail/utils/Constants.dart';
import 'package:ls_retail/utils/NetworkConnection.dart';
import 'package:ls_retail/utils/NetworkOperationManager.dart';
import 'package:ls_retail/utils/Rcode.dart';
import 'package:ls_retail/utils/Rstring.dart';
import 'package:ls_retail/utils/UrlHelper.dart';
import 'package:ls_retail/utils/ntlmclient.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:ntlm/ntlm.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'database/AppDatabase.dart';
import 'database/model/UOM.dart';
import "screens/appSetting.dart";
import 'screens/home.dart';

void main() => runApp(MyApp());

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'EBT Retail POS';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      home: MyLoginPage(),
    );
  }
}

class MyLoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyLoginPage();
  }
}

class _MyLoginPage extends State<MyLoginPage> {
  bool isProgressBarShown = false;
  final _scafffoldkey = GlobalKey<ScaffoldState>();
  NetworkConnectionCheck networkConnectionCheck = new NetworkConnectionCheck();
  var _formKey = GlobalKey<FormState>();

  NTLMClient client;
  String username, password, staffId, store_id, current_date;

  TextEditingController usernameController =
      new TextEditingController(text: "101");
  TextEditingController passwordController =
      new TextEditingController(text: "101");

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    // TODO: implement build
    return Scaffold(
      key: _scafffoldkey,
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: ModalProgressHUD(
        inAsyncCall: isProgressBarShown,
        dismissible: false,
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.fromLTRB(0, 40.0, 0, 80),
                  child: Image.asset('images/ebtlogo.png',
                      width: 140, height: 80, fit: BoxFit.cover),
                ),
                Container(
                    width: queryData.size.width - (queryData.size.width / 8),
                    padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                    child: TextFormField(
                      decoration: InputDecoration(labelText: 'Email'),
                      controller: usernameController,
                    )),
                Container(
                    width: queryData.size.width - (queryData.size.width / 8),
                    padding: const EdgeInsets.fromLTRB(12, 0, 12, 30),
                    child: TextFormField(
                      decoration: InputDecoration(labelText: 'Password'),
                      controller: passwordController,
                    )),
                Container(
                  width: queryData.size.width - (queryData.size.width / 5),
                  child: RaisedButton(
                    color: Colors.blue[700],
                    onPressed: () {
                      networkConnectionCheck
                          .checkInternet(checkInternetForLogin);
                    },
                    child: const Text('LOGIN',
                        style: TextStyle(fontSize: 14, color: Colors.white)),
                  ),
                ),
                Container(
                  width: queryData.size.width - (queryData.size.width / 5),
                  child: RaisedButton(
                    color: Colors.blue[700],
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => AppSetting()),
                      );
                    },
                    child: const Text('App Settings',
                        style: TextStyle(fontSize: 14, color: Colors.white)),
                  ),
                ),
                Container(
                  alignment: Alignment.bottomRight,
                  margin: EdgeInsets.only(right: 16),
                  padding: EdgeInsets.only(top: (queryData.size.height - 590)),
                  child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MyConfigurations()),
                        );
                      },
                      child: Container(
                        padding: EdgeInsets.only(top: 10),
                        // alignment: Alignment.center,
                        height: 45,
                        width: 120,
                        child: Text(
                          "CONFIGURE",
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 16.0,
                            fontStyle: FontStyle.normal,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      )),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void performLogin(String username, String password) async {
    await NetworkOperationManager.testConnection(store_id, client).then((rs) {
      int status = rs.status;
      var responseBody = rs.responseBody;

      if (responseBody == Rstring.LOGIN_SUCCESS) {
        hideProgressBar();
        displaySnackbar(context, "Login success");
        savToPrefs(username);

        getSalesPersonList();
      } else {
        hideProgressBar();
        displaySnackbar(context, "Something went wrong");
      }
    }).catchError((val) {
      hideProgressBar();
      displaySnackbar(context, val);
    });
  }

  @override
  void initState() {
    super.initState();
  }

  checkInternetForLogin(bool isInternetConnected) async {
    //check the status of the form fields and validate
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      //proceed with the process after the form is validated.
      if (isInternetConnected) {
        checkIfNtlmCredentialsExist().then((isExist) {
          if (isExist) {
            checkIfAppIsLicensed().then((isLicensed) {
              if (isLicensed) {
                createNtlmObjectForLogin();
              } else {
                displaySnackbar(context, "Please Configure the license");
              }
            });
          } else {
            displaySnackbar(context, "Please Configure the application");
          }
        });
      } else {
        displaySnackbar(context, "No Internet Connection");
      }
    }
  }

  void createNtlmObjectForLogin() {
    showProgressBar();
    getPrefs().then((val) {
      client = NTLM.initializeNTLM(username, password);

      String etUsername = usernameController.text;
      String etPassword = passwordController.text;
      performLogin(etUsername, etPassword);
    });
  }

  Future<void> getPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = await prefs.getString(ConstantString.USERNAME);
    password = await prefs.getString(ConstantString.PASSWORD);
    if (staffId != null) {
      staffId = await prefs.getString(ConstantString.STAFF_ID);
    }
    if (store_id != null) {
      store_id = await prefs.getString(ConstantString.STORE_ID);
    }
  }

  Future<void> savToPrefs(String staff_id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(ConstantString.STAFF_ID, staff_id);
  }

  Future<bool> checkIfAppIsLicensed() async {
    final prefs = await SharedPreferences.getInstance();
    String license_key = await prefs.getString(ConstantString.LICENSE_KEY);
    if (license_key != null) {
      if (license_key.isNotEmpty) {
        return true;
      }
    }

    return false;
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }

  Future<void> displaySnackbar(BuildContext context, msg) {
    final snackBar = SnackBar(
      content: Text('$msg'),
      duration: const Duration(seconds: 2),
    );
    _scafffoldkey.currentState.showSnackBar(snackBar);
  }

  Future<bool> checkIfNtlmCredentialsExist() async {
    final prefs = await SharedPreferences.getInstance();

    String ntlmPrefTest = prefs.getString(ConstantString.USERNAME);
    if (ntlmPrefTest == null || ntlmPrefTest.isEmpty) {
      return false;
    }

    return true;
  }

  void getSalesPersonList() async {
    showProgressBar();

    var base_url = await UrlHelper.getODATAUrl();
    var url = base_url +
        "/StaffList?\$filter=Employment_Type eq 'both' or Employment_Type eq 'Cashier'&\$format=json";

    client.get(url).then((res) {
      debugPrint("this is url ----------> $url");
      var res_code = res.statusCode;

      if (res_code == Rcode.SUCCESS) {
        hideProgressBar();
        var data = json.decode(res.body);
        var values = data["value"] as List;
        List<SalesPerson> salesPerson = values
            .map<SalesPerson>((json) => SalesPerson.fromJson(json))
            .toList();

        if (salesPerson.length > 0) {
          saveSalesPersonToDatabase(salesPerson);
        }

        openMainScreen();
      } else {
        hideProgressBar();
        openMainScreen();
        displaySnackbar(context, "No responses from server");
      }
    }).catchError((val) {
      openMainScreen();
      displaySnackbar(context, val);
    });
  }

  void openMainScreen() {
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HomeScreen()));
  }

  void saveSalesPersonToDatabase(List<SalesPerson> salesPerson) async {
    final database =
        await $FloorAppDatabase.databaseBuilder(ConstantString.db_name).build();

    await database.salesPersonDao.deleteAll();

    await database.salesPersonDao.insertAll(salesPerson);
  }
}
