class TenderTypeConstants {
  static String CASH = "Cash";
  static String CARDS = "Cards";
  static String CURRENCY = "Currency";
  static String VOUCHER = "Voucher - Physical No barcode";
  static String MOSAMBEE = "Mosambee";


  static int TENDER_CASH = 1;
  static int TENDER_CARDS = 3;
  static int TENDER_CURRENCY = 6;
  static int TENDER_VOUCHER = 7;
  static int TENDER_MOSAMBEE = 8;
}