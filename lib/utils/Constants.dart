class ConstantString {

  static final String db_name = 'db_retail';

  static final String LOGIN_RESPONSE_STRING = "Login successful";
  static final String LICENSE_KEY_RESPONSE = "License Key found successfully!";
  static final String NAV_LICENSE_UPDATE_RESPONSE = "License already registered";
  static final int RESPONSE_CODE = 200;

  //ntlm credentials
  static final String SERVER = "server";
  static final String SERVICE = "service";
  static final String COMPANY = "company";
  static final String ODATA_PORT = "odata_port";
  static final String SOAP_PORT = "soap_port";
  static final String USERNAME = "username";
  static final String PASSWORD = "password";
  static final String STORE_ID = "store_id";
  static final String TERMINAL_ID = "terminal_id";
  static final String SELECTED_FORMAT = "selected_format";
  static final String STAFF_ID = "staff_id";
  static final String STAFF_TYPE_ADMIN = "staff_type_admin";

  //license credentials

  static final String LICENSE_KEY = "license_key";
  static final String EXPIRY_DATE = "expiry";
  static final String CLIENT_EMAIL = "client_email";
  static final String LAST_CHECKED_DATE = "last_checked_date";
  static final String DEVICE_IMEI = "device_imei";

  //app data
  static final String DATA_FORMAT = "data_format";
  static final String CURRENCY = "currency";
  static final String LATITUDE = "latitude";
  static final String LONGITUDE = "longitude";

  static final String RECEIPT_NUMBER = "receipt number";
}
