class ModeConstants {
  static final int MODE_CONSTANT_ONE = 1;
  static final int MODE_CONSTANT_ZERO = 0;
  static final int MODE_CONFIGURATION_TEST = 600;
  static final int MODE_CONFIGURATION_SAVE = 610;

  static final int MODE_BARCODE_SCANNER_BOARDING_PASS = 9001;
  static final int MODE_BARCODE_SCANNER_ADD_ITEM = 9002;
  static final int MODE_BARCODE_SCANNER_ITEM_PRICE = 9003;
  static final int MODE_BARCODE_SCANNER_BILL = 9004;

  static final int MODE_ITEM_OPERATION_UPDATE = 3001;
  static final int MODE_TRANSACTION_OPERATION_SUSPEND = 9011;
  static final int MODE_TRANSACTION_OPERATION_VOID = 90012;
  static final int MODE_TRANSACTION_OPERATION_REFRESH = 90013;
  static final int MODE_TRANSACTION_VOID_TRANSACTION = 90014;

  static final int MODE_CONFIRM_ORDER = 9021;
  static final int MODE_UPDATE_ORDER = 90022;

  static final int MODE_DIALOG_INVOICE_DISCOUNT_PERCENT = 90023;
  static final int MODE_DIALOG_INVOICE_DISCOUNT_AMOUNT = 90024;

  static final int MODE_DIALOG_LINE_DISCOUNT_PERCENT = 90025;
  static final int MODE_DIALOG_LINE_DISCOUNT_AMOUNT = 90026;
  static final int MODE_RECALL_INTENT = 300;
  static final int MODE_NORMAL_INTENT = 301;


  static final int MODE_DATE_PICKER_ISSUE_DATE = 0;
  static final int MODE_DATE_PICKER_EXPIRE_DATE = 1;

  static final int CHANGE_QUANTITY_MODE_ADD = 888;
  static final int CHANGE_QUANTITY_MODE_SUB = 889;

  static final int PRINT_MODE_BLUETOOTH = 100100;
  static final int PRINT_MODE_WIFI = 100101;
  static final int PRINT_MODE_NETWORK = 100102;
  static final int PRINT_MODE_MOSAMBEE = 100103;


  static final int MODE_INTENT_ADD_SALES_PERSON_ON_TRANSACTION = 12;
  static final int MODE_INTENT_ADD_SALES_PERSON_ON_TRANSLINE = 13;

  static final int MODE_PRINT_EMPTY_CART = 223;
  static final int MODE_PRINT_DONOT_EMPTY_CART = 224;

  static final int MODE_SEARCH_ITEM_NUMBER = 441;
  static final int MODE_SEARCH_ITEM_NAME = 442;

  static final int MODE_IS_TEST_PRINT = 711;
  static final int MODE_IS_NOT_TEST_PRINT = 712;

  static final int MODE_SCANNER_CAMERA = 50;
  static final int MODE_SCANNER_MOSAMBEE = 51;
}