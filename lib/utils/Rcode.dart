class Rcode {
  static final int SUCCESS = 200;
  static final int NOT_FOUND = 404;
  static final int SERVER_ERROR = 500;
}
