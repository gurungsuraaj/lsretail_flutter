import 'dart:convert';
import 'package:ls_retail/model/NetworkResponse.dart';
import 'package:ls_retail/utils/DateUtils.dart';
import 'package:xml/xml.dart' as xml;
import 'package:xml2json/xml2json.dart';
import 'package:ls_retail/utils/UrlHelper.dart';
import 'package:ntlm/ntlm.dart';

class NetworkOperationManager {


  static Future<NetworkResponse> testConnection(String store_id,
      NTLMClient client) async {
    NetworkResponse rs = NetworkResponse();
    String response = "";
    var url = await UrlHelper.getSOAPUrl();
    var envelope =
    '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:microsoft-dynamics-schemas/codeunit/MobilePOS">
<soapenv:Body>
<urn:LoginStaff>
<urn:staffID>101</urn:staffID>
<urn:cdPassword>101</urn:cdPassword>
<urn:storeNo>$store_id</urn:storeNo>
</urn:LoginStaff>
</soapenv:Body>
</soapenv:Envelope>''';
    //  var parsed_envelop = xml.encodeXmlText(envelope);

    await client.post(
      url,
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction": "urn:microsoft-dynamics-schemas/codeunit/MobilePOS",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),).then((res) {
      var rawXmlResponse = res.body;
      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);
      var resValue = parsedXml.findAllElements("return_value");
      response = (resValue.map((node) => node.text)).first;

      rs.responseBody = response;
      rs.status = res.statusCode;
    });

    return rs;
  }


  static Future<NetworkResponse> getCurrentDataFrmNav(NTLMClient client) async {
    var url = await UrlHelper.getSOAPUrl();
    NetworkResponse rs = NetworkResponse();

    var envelope =
    '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:microsoft-dynamics-schemas/codeunit/MobilePOS">
<soapenv:Body>
<urn:ReturnDate>
<urn:text>Date</urn:text>
</urn:ReturnDate>
</soapenv:Body>
</soapenv:Envelope>''';
    //  var parsed_envelop = xml.encodeXmlText(envelope);

    await client.post(
      url,
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction":
        "urn:microsoft-dynamics-schemas/codeunit/MobilePOS",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),
    )
        .then((res) {
          print(url);
      var rawXmlResponse = res.body;

// Use the xml package's 'parse' method to parse the response.
      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);
      var resValue = parsedXml.findAllElements("return_value");
      var formattedResVal = resValue.map((node) => node.text);
      var current_date = formattedResVal.first;

      rs.responseBody = current_date;
      rs.status = res.statusCode;
    });
    return rs;
  }

  static Future<NetworkResponse> checkLicenseForValidation(String deviceImei, String licenseKey, String clientEmail, String currentDate, NTLMClient client) async {
    var url = await UrlHelper.getSOAPUrl();
    String app_id = "com.ebtech.lsretail";
    NetworkResponse rs = NetworkResponse();

    var envelope =
    '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:microsoft-dynamics-schemas/codeunit/MobilePOS">
<soapenv:Body>
<urn:CheckLicInfo>
<urn:applicationId>$app_id</urn:applicationId>
<urn:deviceImei>$deviceImei</urn:deviceImei>
<urn:emailID>$clientEmail</urn:emailID>
<urn:licenseKey>$licenseKey</urn:licenseKey>
</urn:CheckLicInfo>
</soapenv:Body>
</soapenv:Envelope>''';
    //  var parsed_envelop = xml.encodeXmlText(envelope);

    await client.post(
      url,
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction":
        "urn:microsoft-dynamics-schemas/codeunit/MobilePOS",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),
    )
        .then((res) {
          print(url);
      var rawXmlResponse = res.body;
      var res_code = res.statusCode;
      print(rawXmlResponse);
      print(res_code);

// Use the xml package's 'parse' method to parse the response.
      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);
      var resValue = parsedXml.findAllElements("return_value");
      var formattedResVal = resValue.map((node) => node.text);
      var checkLicenseResult = formattedResVal.first;

      rs.status = res_code;
      rs.responseBody = checkLicenseResult;
    });
    return rs;
  }

  static Future<NetworkResponse> updateLicenseToNav(String licKey, String email, String expDate, NTLMClient client, String encryptedImei) async {
    var url = await UrlHelper.getSOAPUrl();
    NetworkResponse rs = NetworkResponse();
    String app_id = "com.ebtech.lsretail";

    var envelope =
    '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:microsoft-dynamics-schemas/codeunit/MobilePOS">
<soapenv:Body>
<urn:UpdateLicInfo>
<urn:applicationId>$app_id</urn:applicationId>
<urn:deviceImei>$encryptedImei</urn:deviceImei>
<urn:emailID>$email</urn:emailID>
<urn:licenseKey>$licKey</urn:licenseKey>
</urn:UpdateLicInfo>
</soapenv:Body>
</soapenv:Envelope>''';
    //  var parsed_envelop = xml.encodeXmlText(envelope);

    await client.post(
      url,
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction":
        "urn:microsoft-dynamics-schemas/codeunit/MobilePOS",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),
    )
        .then((res) {
      print(url);
      var rawXmlResponse = res.body;
      print(res.statusCode);
      print(res.body);
      print(res);
      print(res.headers);

// Use the xml package's 'parse' method to parse the response.
      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);
      var resValue = parsedXml.findAllElements("return_value");
      var formattedResVal = resValue.map((node) => node.text);
      var current_date = formattedResVal.first;
      print("curre" +current_date);

      rs.responseBody = current_date;
      rs.status = res.statusCode;
    });
    return rs;
  }


  static Future<NetworkResponse> getReceiptNumberFromNav(String posTerminal, String storeNo, NTLMClient client) async {
    var url = await UrlHelper.getSOAPUrl();
    NetworkResponse rs = NetworkResponse();

    var envelope =
    '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:microsoft-dynamics-schemas/codeunit/MobilePOS">
<soapenv:Body>
<urn:LastRecNo>
<urn:pOSTerminal>$posTerminal</urn:pOSTerminal>
<urn:storeNo>$storeNo</urn:storeNo>
</urn:LastRecNo>
</soapenv:Body>
</soapenv:Envelope>''';

    await client
        .post(
      url,
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction":
        "urn:microsoft-dynamics-schemas/codeunit/MobilePOS",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),
    )
        .then((res) {
      var rawXmlResponse = res.body;

      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);
      var resValue = parsedXml.findAllElements("return_value");
      var formattedResVal = resValue.map((node) => node.text);
      var response_message = formattedResVal.first;

      rs.status = res.statusCode;
      rs.responseBody = response_message;
    });

    return rs;
  }

  static Future<NetworkResponse> createPosTranslineCheck(int entryType, int lineNo, String receiptNo,
      String itemNo, String storeNo, String posTerminal, double decQty, NTLMClient client) async {

    var url = await UrlHelper.getSOAPUrl();
    NetworkResponse rs = NetworkResponse();

    var envelope =
    '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:microsoft-dynamics-schemas/codeunit/MobilePOS">
<soapenv:Body>
<urn:CreatePosTransLineCheck>
<urn:entryType>$entryType</urn:entryType>
<urn:lineNo>$lineNo</urn:lineNo>
<urn:receiptNo>$receiptNo</urn:receiptNo>
<urn:itemNo>$itemNo</urn:itemNo>
<urn:storeNo>$storeNo</urn:storeNo>
<urn:pOSTerminal>$posTerminal</urn:pOSTerminal>
<urn:decQty>$decQty</urn:decQty>
</urn:CreatePosTransLineCheck>
</soapenv:Body>
</soapenv:Envelope>''';

    await client
        .post(
      url,
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction":
        "urn:microsoft-dynamics-schemas/codeunit/MobilePOS",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),
    )
        .then((res) {
      var rawXmlResponse = res.body;

      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);
      var resValue = parsedXml.findAllElements("return_value");
      var formattedResVal = resValue.map((node) => node.text);
      var response_message = formattedResVal.first;

      rs.status = res.statusCode;
      rs.responseBody = response_message;
    });

    return rs;
  }

  static Future<NetworkResponse> createPosTransaction(String receiptNo, String retriveRecNo,
      String storeNo, String posTerminal, NTLMClient client) async{

    String emptyString = "";

    var url = await UrlHelper.getSOAPUrl();
    NetworkResponse rs = NetworkResponse();

    String transDate = DateUtils.getTransDate();
    String transTime = DateUtils.getTransTime();

    var envelope =
    '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:microsoft-dynamics-schemas/codeunit/MobilePOS">
<soapenv:Body>
<urn:CreatePosTransaction>
<urn:receiptNo>$receiptNo</urn:receiptNo>
<urn:storeNo>$storeNo</urn:storeNo>
<urn:transDate>$transDate</urn:transDate>
<urn:orgDate>$transDate</urn:orgDate>
<urn:transTime>$transTime</urn:transTime>
<urn:netAmt>0</urn:netAmt>
<urn:grossAmt>0</urn:grossAmt>
<urn:selltoContactNo>$emptyString</urn:selltoContactNo>
<urn:salesType>$emptyString</urn:salesType>
<urn:commentText>$emptyString</urn:commentText>
<urn:noOfCovers>0</urn:noOfCovers>
<urn:tableNo>0</urn:tableNo>
<urn:pOSTerminal>$posTerminal</urn:pOSTerminal>
<urn:retriveRecNo>$retriveRecNo</urn:retriveRecNo>
</urn:CreatePosTransaction>
</soapenv:Body>
</soapenv:Envelope>''';

    await client
        .post(
      url,
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction":
        "urn:microsoft-dynamics-schemas/codeunit/MobilePOS",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),
    )
        .then((res) {
      var rawXmlResponse = res.body;

      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);
      var resValue = parsedXml.findAllElements("return_value");
      var formattedResVal = resValue.map((node) => node.text);
      var response_message = formattedResVal.first;

      rs.status = res.statusCode;
      rs.responseBody = response_message;
    });

    return rs;


  }
}
