class StringUtils{

  static String encodeHashInString(String valueString){

    var buffer = new StringBuffer();

    for(int i =0; i < valueString.length; i++){
      if(valueString[i] == '#'){
        buffer.write("%23");
      }else{
        buffer.write(valueString[i]);
      }
    }

    return buffer.toString();

  }
}