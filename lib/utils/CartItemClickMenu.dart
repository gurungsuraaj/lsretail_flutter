class CartItemClickMenu {
  static final String ADD_SALES_PERSON_TRANSACTION = "Sales person transaction";
  static final String ADD_SALES_PERSON_TRANSLINES = "Sales person transline";
  static final String ADD_LINE_DISCOUNT_PERCENT = "Add line discount percent";
  static final String ADD_LINE_DISCOUNT_AMOUNT = "Add line discount amount";
  static final String ADD_FIXED_DISCOUNT = "Add fixed discount";
}