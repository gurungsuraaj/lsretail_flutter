class CartConstants {
  static final int CART_NORMAL = 0;
  static final int CART_RETURN = 1;

  static final int CART_ITEM_IS_UPDATED_IN_NAV = 1;
  static final int CART_ITEM_IS_LOCAL_NOT_UPDATED_IN_NAV = 0;

  static final int CART_REFRESH = 3;

  static final int CARTPOSTRANSLINE_ITEM = 0;
  static final int CARTPOSTRANSLINE_COUPON = 6;

  static final String ENTRY_TYPE_ITEM = "Item";
  static final String ENTRY_TYPE_COUPON = "Coupon";
}