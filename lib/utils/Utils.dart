import 'package:shared_preferences/shared_preferences.dart';

import 'Constants.dart';

class Utils {

  static Future<String> amountConverter(String amount) async{

    getCurrencyFromPrefs().then((currency) {

      double doubleAmt = double.parse(amount);
      assert(doubleAmt is double);

      var trimmedAmt = doubleAmt.toStringAsFixed(3);
      String newAmt =  currency + " " +trimmedAmt;
      print("newAmt -----------> $newAmt");
      return newAmt;

    });
  }

  static Future<String> getCurrencyFromPrefs() async {
    final prefs = await SharedPreferences.getInstance();

    return await prefs.getString(ConstantString.CURRENCY);
  }
}
