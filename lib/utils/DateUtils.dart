class DateUtils{
  
  static String getTransDate(){
    
    DateTime dateTime = new DateTime.now();
    String dateTimeString = dateTime.toString();

    return dateTimeString.substring(0, 10);
  }

  static String getTransTime(){

    DateTime dateTime = new DateTime.now();
    String dateTimeString = dateTime.toString();

    return dateTimeString.substring(11, 19);
  }
}