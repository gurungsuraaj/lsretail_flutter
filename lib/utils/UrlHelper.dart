import 'package:ls_retail/utils/Constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UrlHelper {


  static Future<String> getSOAPUrl() async{

    final prefs = await SharedPreferences.getInstance();
    String server = prefs.getString(ConstantString.SERVER);
    String service = prefs.getString(ConstantString.SERVICE);
    String soap_port = prefs.getString(ConstantString.SOAP_PORT);
    String company = prefs.getString(ConstantString.COMPANY);

    String encodedCompany = Uri.encodeFull(company);

   return "http://" + server + ":" + soap_port + "/" + service + "/WS/" + encodedCompany + "/Codeunit/MobilePOS";

  }

/*  static Future<String> getSOAPUrlForCreationOfDelivery() async{

    final prefs = await SharedPreferences.getInstance();
    String server = prefs.getString(ConstantString.SERVER);
    String service = prefs.getString(ConstantString.SERVICE);
    String soap_port = prefs.getString(ConstantString.SOAP_PORT);
    String company = prefs.getString(ConstantString.COMPANY);

    String encodedCompany = Uri.encodeFull(company);

    return "http://" + server + ":" + soap_port + "/" + service + "/WS/" + encodedCompany + "/Codeunit/MobilePOS";

  }*/

  static Future<String> getODATAUrl() async{

    final prefs = await SharedPreferences.getInstance();
    String server = prefs.getString(ConstantString.SERVER);
    String service = prefs.getString(ConstantString.SERVICE);
    String soap_port = prefs.getString(ConstantString.SOAP_PORT);
    String odata_port = prefs.getString(ConstantString.ODATA_PORT);
    String company = prefs.getString(ConstantString.COMPANY);

    String encodedCompany = Uri.encodeFull(company);

    return "http://" + server + ":" + odata_port + "/" + service + "/ODATA/Company" + "('" +encodedCompany + "')";

  }

}
