import 'package:flutter/material.dart';
import 'package:ls_retail/utils/CartConstants.dart';
import 'package:ls_retail/database/AppDatabase.dart';
import 'package:ls_retail/model/SalesPerson.dart';
import 'package:ls_retail/utils/CompareDate.dart';
import 'package:ls_retail/utils/Constants.dart';
import 'package:ls_retail/utils/NetworkOperationManager.dart';
import 'package:ls_retail/utils/Rcode.dart';
import 'package:ls_retail/utils/Rstring.dart';
import 'package:ls_retail/utils/ntlmclient.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:ntlm/ntlm.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'CartActivity.dart';
import 'priceCheck.dart';
import 'ItemLookUp.dart';
import 'shopCollect.dart';

class MyButton extends StatelessWidget {
  //Constructor. { here donte that they are optional, we can only use as new MyCard}

  const MyButton({@required this.title, @required this.onPressed});
  final String title;
  final Function onPressed;
  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: const EdgeInsets.only(top: 8),
      child: ButtonTheme(
        minWidth: 350.0,
        height: 45.0,
        buttonColor: Colors.blue[800],
        child: RaisedButton(
          onPressed: this.onPressed,
          child: Text(
            '$title',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }
}

class HomeScreen extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

enum ConfirmAction { CANCEL, ACCEPT }

class HomeState extends State<HomeScreen> {
  String lastCheckedDate, deviceImei, clientEmail, licenseKey, posTerminal, storeNo;
  String _text = "";
  bool isProgressBarShown = false;
  final _scafffoldkey = GlobalKey<ScaffoldState>();

  NTLMClient client;
  String username, password, staffId, currentDate;

  void _dialogResult(ConfirmAction value) {
    print('Selected Value');
  }

  void _showAlert(String value) {
    AlertDialog dialog = new AlertDialog(
      content: Text(
        "Return Item",
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            _dialogResult(ConfirmAction.CANCEL);
          },
          child: new Text("Cancel"),
        ),
        new FlatButton(
          onPressed: () {
            _dialogResult(ConfirmAction.CANCEL);
          },
          child: new Text("Without Bill"),
        ),
        new FlatButton(
          onPressed: () {
            _dialogResult(ConfirmAction.CANCEL);
          },
          child: new Text("With Bill"),
        )
      ],
    );
    showDialog(context: context, child: dialog);
  }

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        key: _scafffoldkey,
        appBar: AppBar(
          title: Text('Dashboard'),
          backgroundColor: Colors.blue[800],
        ),
        body: ModalProgressHUD(
    inAsyncCall: isProgressBarShown,
    dismissible: false,
            child: Container(
            margin: const EdgeInsets.only(top: 20),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(bottom: 50),
                    child: Image.asset('images/ebtlogo.png',
                        width: 140, height: 80, fit: BoxFit.cover),
                  ),
                  MyButton(
                    title: 'SALE',
                    onPressed: () {
                      getReceiptNumber(CartConstants.CART_NORMAL);
                    },
                  ),
                  MyButton(
                    title: 'PRICE CHECK',
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PriceCheck()),
                      );
                    },
                  ),
                  MyButton(
                    title: 'RETURN ITEM',
                    onPressed: () {
                      _showAlert(_text);
                    },
                  ),
                  MyButton(
                    title: 'RECALL TRANSACTION',
                    onPressed: () {
                    },
                  ),
                  MyButton(
                    title: 'PRINT LAST RECEIPT',
                    onPressed: () {
                    },
                  ),
                  MyButton(
                    title: 'DAY CLOSE',
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ShopCollect()),
                      );
                    },
                  ),
                ],
              ),
            )),
      ),
    ),
    );
  }

  @override
  void initState() {
      super.initState();

      getPrefs().then((val) {
        client = NTLM.initializeNTLM(username, password);
      }).then((val) {
        checkLicenseExpiry();
      });
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }

  Future<void> getPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = await prefs.getString(ConstantString.USERNAME);
    password = await prefs.getString(ConstantString.PASSWORD);
    deviceImei = await prefs.getString(ConstantString.DEVICE_IMEI);
    clientEmail = await prefs.getString(ConstantString.CLIENT_EMAIL);
    licenseKey = await prefs.getString(ConstantString.LICENSE_KEY);
    lastCheckedDate = await prefs.getString(ConstantString.LAST_CHECKED_DATE);
    posTerminal = await prefs.getString(ConstantString.TERMINAL_ID);
    storeNo = await prefs.getString(ConstantString.STORE_ID);
  }

  Future<void> displaySnackbar(BuildContext context, msg) {
    final snackBar = SnackBar(
      content: Text('$msg'),
      duration: const Duration(seconds: 2),
    );
    _scafffoldkey.currentState.showSnackBar(snackBar);
  }

  void checkLicenseExpiry() {
    getCurrentDateFromNav();
  }

  void getCurrentDateFromNav() async {
    showProgressBar();

    NetworkOperationManager.getCurrentDataFrmNav(client).then((val){
      debugPrint("came to get current date from nav");
      var res_body = val.responseBody;
      var response_code = val.status;

      // ignore: unrelated_type_equality_checks
      if (response_code == Rcode.SUCCESS) {

        hideProgressBar();
        if (lastCheckedDate == "") {
          debugPrint("last checked date is empty");
          currentDate = res_body;
          checkLicenseForValidation(currentDate);
        } else {
          debugPrint("last checked date is expired..its not empty");
          bool isLicenseExpired =
          CompareDate.CompareDateFormat(currentDate, lastCheckedDate);
          if (isLicenseExpired) {
            debugPrint("is it really expired??");
            displaySnackbar(context, "License Status : Check for Validation");
            checkLicenseForValidation(currentDate);
          } else {
            // displaySnackbar(context, "License Status : Activated");
          }
        }
      } else {
        hideProgressBar();
        displaySnackbar(context, Rstring.something_went_wrong);
      }
    }).catchError((val) {
      hideProgressBar();
      displaySnackbar(context, val);
    });

  }

  void checkLicenseForValidation(String current_date) {
    showProgressBar();

    NetworkOperationManager.checkLicenseForValidation(deviceImei, licenseKey, clientEmail, currentDate, client).then((val){

      debugPrint("Device IMEI => $deviceImei");
      debugPrint("license => $licenseKey");
      debugPrint("client email => $clientEmail");
      debugPrint("current date => $currentDate");

      // #IMEI 2 => d5c37de23d0b0aaf3e507c30b574793f (Suraj Dharel)

      if(val.status == Rcode.SUCCESS){
        hideProgressBar();
        if (val.responseBody == Rstring.CHECK_LICENSE_SUCESS_RESPONSE) {
          debugPrint("response matched");
          displaySnackbar(context, "License Status : Activated");
          saveDateToPrefs(current_date);
        } else {
          displaySnackbar(context, "License Failure Response");
          debugPrint("response did not matched");
        }
      }
      else {
        hideProgressBar();
        displaySnackbar(context, Rstring.something_went_wrong);
      }
    });
  }

  Future<void> saveDateToPrefs(String current_date) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(ConstantString.LAST_CHECKED_DATE, current_date);
  }

  Future<void> saveReceiptToPrefs(String current_ReceiptNumber) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(ConstantString.RECEIPT_NUMBER, current_ReceiptNumber);
  }

  void getReceiptNumber(int cartMode) {

    fetchnewReceiptNumberWithBoardingPassCheck(cartMode);
  }

  void fetchnewReceiptNumberWithBoardingPassCheck(int cartMode) {
    showProgressBar();

    NetworkOperationManager.getReceiptNumberFromNav(posTerminal,storeNo,client).then((val){

      if(val.status == Rcode.SUCCESS){
        String receiptNumber = val.responseBody;
        hideProgressBar();
          displaySnackbar(context, "Receipt number : $receiptNumber");
          saveReceiptToPrefs(receiptNumber);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => CartActivity()),
        );

        }
      else {
        hideProgressBar();
        displaySnackbar(context, Rstring.something_went_wrong);
      }
    }).catchError((val) {
      hideProgressBar();
      displaySnackbar(context, val);
    });

  }
}

