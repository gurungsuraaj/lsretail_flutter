import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ShopCollect extends StatefulWidget {
  ShopCollect({Key key}) : super(key: key);

  ShopCollectState createState() => ShopCollectState();
}

class ShopCollectState extends State<ShopCollect> {
  DateTime selectedDate = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    return Container(
      child: Scaffold(
          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            title: Text("Shop and Collect"),
            automaticallyImplyLeading: true,
            backgroundColor: Colors.blue[800],
          ),
          body: ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    width: queryData.size.width,
                    height: 60.0,
                    padding: EdgeInsets.fromLTRB(10, 8, 10, 0),
                    child: Card(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(
                              "Receipt No.",
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(right: 10),
                            child: Text(
                              "001928888888828828",
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: queryData.size.width,
                    height: 60.0,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
                    child: Card(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(
                              "Post Terminal",
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(right: 10),
                            child: Text(
                              "S1001",
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: queryData.size.width,
                    height: 80.0,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
                    child: Card(
                        child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 12),
                      child: TextFormField(
                        decoration: InputDecoration(labelText: 'Flight no.'),
                      ),
                    )),
                  ),
                  Container(
                    width: queryData.size.width,
                    height: 60.0,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
                    child: Card(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(
                              "Return Terminal",
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(right: 10),
                            child: Text(
                              "SELECT >",
                              style: TextStyle(
                                  color: Colors.blue[600],
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(24, 0, 24, 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: queryData.size.width / 1.8,
                          height: 60,
                          child: TextFormField(
                            decoration:
                                InputDecoration(labelText: 'Arrival Date'),
                          ),
                        ),
                        Container(
                          width: 70,
                          height: 50,
                          child: RaisedButton(
                            color: Colors.blue[700],
                            onPressed: () => _selectDate(context),
                            child: IconButton(
                              icon: Icon(
                                FontAwesomeIcons.calendar,
                                color: Colors.white,
                              ),
                              onPressed: null,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(24, 0, 24, 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: queryData.size.width / 1.8,
                          height: 50,
                          child: TextFormField(
                            decoration:
                                InputDecoration(labelText: 'Arrival Time'),
                          ),
                        ),
                        Container(
                          width: 70,
                          height: 50,
                          child: RaisedButton(
                            color: Colors.blue[700],
                            onPressed: () => _selectDate(context),
                            child: IconButton(
                              icon: Icon(
                                FontAwesomeIcons.clock,
                                color: Colors.white,
                              ),
                              onPressed: null,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: queryData.size.width,
                    height: 80.0,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
                    child: Card(
                        child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 12),
                      child: TextFormField(
                        decoration: InputDecoration(labelText: 'Customer Name'),
                      ),
                    )),
                  ),
                  Container(
                    width: queryData.size.width,
                    height: 80.0,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
                    child: Card(
                        child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 12),
                      child: TextFormField(
                        decoration: InputDecoration(labelText: 'Mobile No.'),
                      ),
                    )),
                  ),
                  Container(
                    width: queryData.size.width,
                    height: 80.0,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
                    child: Card(
                        child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 12),
                      child: TextFormField(
                        decoration: InputDecoration(labelText: 'Email'),
                      ),
                    )),
                  ),
                  Container(
                    width: queryData.size.width,
                    height: 80.0,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
                    child: Card(
                        child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 12),
                      child: TextFormField(
                        decoration: InputDecoration(labelText: 'Comment.'),
                      ),
                    )),
                  ),
                  Container(
                    width: queryData.size.width / 2.3,
                    padding: EdgeInsets.symmetric(vertical: 15),
                    child: RaisedButton(
                      color: Colors.blue[700],
                      onPressed: () {},
                      child: const Text('SUBMIT',
                          style: TextStyle(fontSize: 14, color: Colors.white)),
                    ),
                  ),
                ],
              )
            ],
          )),
    );
  }
}
