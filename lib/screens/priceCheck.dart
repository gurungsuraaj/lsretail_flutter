import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

// void main() => runApp(MyApp());

class PriceCheck extends StatefulWidget {
  @override
  PriceCheckState createState() => PriceCheckState();
}


class PriceCheckState extends State<PriceCheck> {
  var _currencies = ['Rupees', 'Dollar', 'Pounds'];
  var _currentItemSelected = 'Rupees';

  void _dropDownItemSelected(String newValueSelected) {
    setState(() {
      this._currentItemSelected = newValueSelected;
    });
  }



  

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('EBT Retail POS'),
          backgroundColor: Colors.blue[800],
          actions: <Widget>[
            IconButton(
              icon: new Icon(FontAwesomeIcons.barcode),
              onPressed: () {},
            )
          ],
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.fromLTRB(30, 30, 0, 0),
                child: Text(
                  "Price Checker",
                  style: TextStyle(color: Colors.grey[700]),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(30, 0, 20, 12),
                alignment: Alignment.center,
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Item Number*',
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.fromLTRB(30, 12, 0, 4),
                child: Text(
                  "Unit of Measurement",
                  style: TextStyle(color: Colors.grey[700]),
                ),
              ),
              Container(
                height: 65,
                padding: EdgeInsets.fromLTRB(30, 2, 120, 4),
                child: DropdownButton<String>(
                  isExpanded: true,
                  items: _currencies.map((String dropDownStringItem) {
                    return DropdownMenuItem<String>(
                      value: dropDownStringItem,
                      child: Text(dropDownStringItem),
                    );
                  }).toList(),
                  onChanged: (String newValueSelected) {
                    _dropDownItemSelected(newValueSelected);
                  },
                  value: _currentItemSelected,
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.fromLTRB(30, 12, 0, 4),
                child: Text(
                  "Unit of Measurement",
                  style: TextStyle(color: Colors.grey[700]),
                ),
              ),
              Container(
                height: 65,
                padding: EdgeInsets.fromLTRB(30, 2, 120, 4),
                child: DropdownButton<String>(
                  isExpanded: true,
                  items: _currencies.map((String dropDownStringItem) {
                    return DropdownMenuItem<String>(
                      value: dropDownStringItem,
                      child: Text(dropDownStringItem),
                    );
                  }).toList(),
                  onChanged: (String newValueSelected) {
                    _dropDownItemSelected(newValueSelected);
                  },
                  value: _currentItemSelected,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 8),
                child: ButtonTheme(
                  minWidth: 350.0,
                  height: 38.0,
                  buttonColor: Colors.blue[800],
                  child: RaisedButton(
                    onPressed: () {},
                    child: Text(
                      "GET PRICE",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
