import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ls_retail/database/model/CartPosTransline.dart';
import 'package:ls_retail/database/model/Item.dart';
import 'package:ls_retail/utils/Constants.dart';
import 'package:ls_retail/utils/ModeConstants.dart';
import 'package:ls_retail/utils/NetworkOperationManager.dart';
import 'package:ls_retail/utils/Rcode.dart';
import 'package:ls_retail/utils/UrlHelper.dart';
import 'package:ls_retail/utils/ntlmclient.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:ntlm/ntlm.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ItemLookUp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ItemLookUpActivity();
  }
}

class ItemLookUpActivity extends State<ItemLookUp> {
  String _text = "";
  bool isProgressBarShown = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<Item> itemList = new List<Item>();

  NTLMClient client;
  String username, password, staffId, currentDate, currency;
  
  String receiptNo, storeNo, posTerminal;

  TextEditingController searchController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    getPrefs().then((val) {
      client = NTLM.initializeNTLM(username, password);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        appBar: AppBar(
          key: _scaffoldKey,
          automaticallyImplyLeading: true,
          title: Text("EBT Retail POS"),
          backgroundColor: Colors.blue[700],
          actions: <Widget>[
            IconButton(icon: Icon(FontAwesomeIcons.barcode), onPressed: () {})
          ],
        ),
        body: ModalProgressHUD(
          inAsyncCall: isProgressBarShown,
          dismissible: false,
          child: TabBarView(
            children: <Widget>[
              Container(
                // padding: const EdgeInsets.fromLTRB(18, 18, 18, 0),
                margin: EdgeInsets.fromLTRB(18, 20, 18, 0),
                child: Column(
                  children: <Widget>[
                    TextField(
                      decoration: InputDecoration(labelText: 'Search'),
                      onEditingComplete: getItem(),
                      controller: searchController,
                      onChanged: (text) {
                      },
                    ),
                    RaisedButton(
                      onPressed: () {
                        var text = searchController.text;
                          debugPrint("this is text ===> $text");
                          if (text.isNotEmpty) {
                            if (text.contains(new RegExp(r'[A-Za-z]'))) {
                              getItemsFromNav(
                                  text, ModeConstants.MODE_SEARCH_ITEM_NAME);
                            } else {
                              getItemsFromNav(
                                  text, ModeConstants.MODE_SEARCH_ITEM_NUMBER);
                            }
                          }
                      },
                      textColor: Colors.blue,
                      color: Colors.white,
                      child: new Text(
                        "SEARCH",
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: itemList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () {
                                
                                getPrefsForCreatePosTransaction().then((val){
                                  createPosTransaction();
                                });

                                /*   var id = driverslist[index].ID;
                                  var orderNo = delivery.Order_No;
                                  AssignDriver(id,orderNo, context);*/
                              },
                              child: Card(
                                color: Colors.white,
                                child: Column(
                                  children: <Widget>[
                                        Container(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Expanded(
                                                child: Container(
                                                  padding: EdgeInsets.fromLTRB(
                                                      8.0, 16.0, 0, 16.0),
                                                  child: Text(" "+
                                                      itemList[index]
                                                          .description,
                                                    style:
                                                    TextStyle(fontSize: 16.0),
                                                  ),
                                                ),
                                              ),

                                              Container(
                                                padding: EdgeInsets.fromLTRB(
                                                    0, 16.0, 8.0, 16.0),
                                                child: Text(" "+currency+" "+
                                                    itemList[index]
                                                        .unitPrice,
                                                  style:
                                                  TextStyle(fontSize: 16.0),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void getItemsFromNav(String text, int mode) async {
    showProgressBar();
    var url;
    var base_url = await UrlHelper.getODATAUrl();
    if (mode == ModeConstants.MODE_SEARCH_ITEM_NAME) {
      url = base_url +
          "/ItemList?\$filter=substringof(Description,'" +
          text +
          "')&\$format=json";
    } else if (mode == ModeConstants.MODE_SEARCH_ITEM_NUMBER) {
      url = base_url +
          "/ItemList?\$filter=substringof(No,'" +
          text +
          "')&\$format=json";
    }

    client.get(url).then((res) async {
      debugPrint("this is url ----------> $url");
      var res_code = res.statusCode;

      if (res_code == Rcode.SUCCESS) {
        hideProgressBar();
        var data = json.decode(res.body);
        var values = data["value"] as List;
        debugPrint("this is values ==> $values");

        setState(() {
          itemList = values.map<Item>((json) => Item.fromJson(json)).toList();
        });

        var a = itemList[0].description;
        debugPrint("this is item ==> $a");
      } else {
        hideProgressBar();
      }
    }).catchError((val) {
      hideProgressBar();
    });
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }

  Future<void> getPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = await prefs.getString(ConstantString.USERNAME);
    password = await prefs.getString(ConstantString.PASSWORD);
    currency = await prefs.getString(ConstantString.CURRENCY);
  }

  getItem() {
    String s = searchController.text;
    debugPrint("editing completed $s");
  }

  void createPosTransaction() async{
    
    await NetworkOperationManager.createPosTransaction(receiptNo, "", storeNo, posTerminal, client).then((rs){

      int status = rs.status;
      var responseBody = rs.responseBody;

      debugPrint("createPOsTransaction ----------> $status");
      debugPrint("createPOsTransaction ----------> $responseBody");
    });
    
  }

  Future<void> getPrefsForCreatePosTransaction() async {
    
    final prefs = await SharedPreferences.getInstance();
    
    receiptNo = await prefs.getString(ConstantString.RECEIPT_NUMBER);
    storeNo = await prefs.getString(ConstantString.STORE_ID);
    posTerminal = await prefs.getString(ConstantString.TERMINAL_ID);
    
  }

/*  void displaySnackbar(BuildContext context, msg) {
    final snackBar = SnackBar(
      content: Text('$msg'),
      duration: const Duration(seconds: 2),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }*/
}
