import 'dart:convert';

import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart' as crypto;
import 'package:ls_retail/model/LicenseResponse.dart';
import 'package:ls_retail/utils/Constants.dart';
import 'package:ls_retail/utils/NetworkConnection.dart';
import 'package:ls_retail/utils/NetworkOperationManager.dart';
import 'package:ls_retail/utils/ntlmclient.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart' as http;
import 'package:imei_plugin/imei_plugin.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:ntlm/ntlm.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:xml/xml.dart' as xml;

class GetLicenseKey extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'EBT Delivery',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: GetLicense(),
    );
  }
}

class GetLicense extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _GetLicense();
  }
}

/** main state **/
class _GetLicense extends State<GetLicense> {
  bool isProgressBarShown = false;

  NetworkConnectionCheck networkManager = new NetworkConnectionCheck();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String licensekey = "";
  var encryptedImei;
  static var device_imei = "";
  NTLMClient client;
  String IMEI;
  Future<String> device_imei_future;
  TextEditingController emailController =
      new TextEditingController(text: "nipunasewa@gmail.com");



  @override
  void initState() {
    super.initState();
    device_imei_future = ImeiPlugin.getImei.then((val) {
      setState(() {
        device_imei = val;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("License Manager"),
        ),
        body: ModalProgressHUD(
            inAsyncCall: isProgressBarShown,
            dismissible: false,
            child: Column(
              children: <Widget>[
                new Container(
                  margin:
                      EdgeInsets.only(left: 10, top: 60, right: 10, bottom: 10),
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          decoration:
                              InputDecoration(hintText: 'Client Email ID'),
                          controller: emailController,
                        ),
                      )
                    ],
                  ),
                ),
                new Container(
                  margin:
                      EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 10),
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "IMEI :",
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "$device_imei",
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  margin:
                      EdgeInsets.symmetric(horizontal: 10.0, vertical: 16.0),
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          child: (new RaisedButton(
                        onPressed: () {
                          networkManager
                              .checkInternet(checkInternetForGetLicenseKey);
                        },
                        textColor: Colors.white,
                        color: Colors.blue,
                        padding: const EdgeInsets.all(8.0),
                        child: new Text(
                          "Get License",
                        ),
                      ))),
                    ],
                  ),
                ),
              ],
            )));
  }

  checkInternetForGetLicenseKey(bool isInternetConnected) {
    if (isInternetConnected) {
      getLicenseKey();
    } else {
      displaySnackbar(context, "No Internet Connection");
    }
  }

  getLicenseKey() {
    String app_id = "com.ebtech.ebtdelivery";
    String bearer_token =
        "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjZlNWVlOWQxOWQ0OTY3ZGQwMjAwMjBmNjdmZjQxMTYzMTAyYmZlY2IyMTY0ZDcyYWViY2EyNmEzYmFjMDVjNTI1NjIzYTQzMDU1OTk5YzFjIn0.eyJhdWQiOiIxIiwianRpIjoiNmU1ZWU5ZDE5ZDQ5NjdkZDAyMDAyMGY2N2ZmNDExNjMxMDJiZmVjYjIxNjRkNzJhZWJjYTI2YTNiYWMwNWM1MjU2MjNhNDMwNTU5OTljMWMiLCJpYXQiOjE1NTI5ODE0NzgsIm5iZiI6MTU1Mjk4MTQ3OCwiZXhwIjoxNTg0NjAzODc4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.PWywtkDBPHVU5BB6xsIlB0AyYAPAfzQaqtUV-p6s5OFerc6FHticNvoHa-eP68QbzvDonZEIdsOYXlgx2ooe7ARcSplhxvREnSvQsbTxQYbCtWS5u-dDZPgiMB8msWyE5luVt-606itrv5dQ6qrkFi_aob4LNAdvfVQncObha9KV7eEjK0T3NdhHZNo_trDF4dcApkWCmeaYRljz5ZGSvPet3bltIC-eZpeg0JElAX9Hq3rgAzP47aXRO0w2YM1RfNQovEyb-8NGImzu0dDtyIVwcbAiMym5sLsl3yXDBrnniCd692ss1tpe48SOAA150Qb9WFQ1H6Ir61TX8Qt6pvg7LJR4yEWmxQ697UbOx3jYyqL_lfiG2b0eMudzlYHp1EbL2r6Hl7eBHYP1POpIiLgBjUGh6UgmrEda9EAMGDYbC3Ucsg0hz8l7963h59jI98mrn9czLlWtAdS6HDiwBKmg7IL2ixL064M5ZExSDqMz7dHh7_p8-LfTLMMjAK6pDeBCGj-Kmp7jZhmAdBNESxGznkyro3t7TIBas_uqIGxcIDDcq7mf175kdev73y-lzb_LqNRB6ABfVkM260O6J_qQ1k0FYtftBSHpD_fcy3mzvLEMB9MqBaOMvYjFg1n2EwfFP9vDW-ZqkCHSBfyyNNp6DGlU9hM6I3YYUNX7dX4";
    String deviceImei = "EBT" + "$device_imei";
    //   encryptedImei = getEncryptedImei(deviceImei);

    encryptedImei = "d5c37de23d0b0aaf3e507c30b574793f";
    debugPrint("This is encrypted IMEI --------> $encryptedImei");
    postLicense(app_id, bearer_token, encryptedImei);
  }

  String getEncryptedImei(String deviceImei){
    var content = new Utf8Encoder().convert(deviceImei);
    var md5 = crypto.md5;
    var digest = md5.convert(content);
    return hex.encode(digest.bytes);
  }

  postLicense(String app_id, String token, String imei) async {
    showProgressBar();

    LicenseResponse licenseResponse = new LicenseResponse("", "", "");
    String client_email = emailController.text;

    Map<String, String> body = {
      "imei": "$imei",
      "application_id": "$app_id",
      "client_email": "$client_email",
    };

    Map<String, String> header = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Authorization": "$token",
    };

    http.Response response = await http.post(
        'https://license.ebt-me.com/api/license',
        headers: header,
        body: body);
    var statusCode = response.statusCode;

    hideProgressBar();

    if (statusCode == ConstantString.RESPONSE_CODE) {
      final Map parsed = json.decode(response.body);
      licenseResponse = LicenseResponse.fromJson(parsed);
      var message = licenseResponse.message;
      var licensekey = licenseResponse.license_key;
      var expiryDate = licenseResponse.expiry_date;
      debugPrint("license key ---> $licensekey");

      if (message == ConstantString.LICENSE_KEY_RESPONSE) {
        updateLicenseToNav(licensekey, expiryDate, client_email);
      } else {
        displaySnackbar(context, message);
      }
    } else {
      displaySnackbar(context, "Something went wrong. Please try again.");
    }
  }

  void displaySnackbar(BuildContext context, msg) {
    final snackBar = SnackBar(content: Text('$msg'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void updateLicenseToNav(String licKey, String expDate, String email) async {
    showProgressBar();

    client = NTLM.initializeNTLM("Nipuna", "Nepal@123");

    NetworkOperationManager.updateLicenseToNav(licKey,email,expDate, client, encryptedImei)
        .then((val) {
      var response_message = val.responseBody;

      debugPrint("this is response ==========> $response_message");
      if (response_message == ConstantString.NAV_LICENSE_UPDATE_RESPONSE) {
        hideProgressBar();
        displaySnackbar(context, response_message);
        saveLicenseCredentials(licKey, expDate, email);
      } else {
        hideProgressBar();
        displaySnackbar(context, "No response from server");
      }
    }).catchError((val) {
      hideProgressBar();
      displaySnackbar(context, val);
    });
  }

  Future<void> saveLicenseCredentials(
      String licKey, String expDate, String email) async {
    final prefs = await SharedPreferences.getInstance();

    await prefs.setString(ConstantString.LICENSE_KEY, licKey);
    await prefs.setString(ConstantString.EXPIRY_DATE, expDate);
    await prefs.setString(ConstantString.CLIENT_EMAIL, email);
    await prefs.setString(ConstantString.LAST_CHECKED_DATE, "");
    await prefs.setString(ConstantString.DEVICE_IMEI, encryptedImei);
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }
}
