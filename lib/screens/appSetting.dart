import 'package:flutter/material.dart';

class AppSetting extends StatefulWidget {
  @override
  AppSettingState createState() => AppSettingState();
}

class AppSettingState extends State<AppSetting> {
  bool val = false;

  onSwitchValueChanged(bool newVal) {
    setState(() {
      val = newVal;
    });
  }

  String nameCity = "";
  var _currencies = ['Rupees', 'Dollar', 'Pounds'];
  var _currentItemSelected = 'Rupees';

  void _dropDownItemSelected(String newValueSelected) {
    setState(() {
      this._currentItemSelected = newValueSelected;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("App Setting"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                "App Setting",
                style: TextStyle(
                    fontWeight: FontWeight.w600, color: Colors.grey[700]),
              ),
              color: Colors.grey[300],
              width: double.infinity,
              height: 30.0,
              alignment: Alignment.centerLeft,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.fromLTRB(12, 12, 0, 3),
                        child: Text(
                          "Boarding Pass Scanner",
                          style:
                              TextStyle(fontSize: 18, color: Colors.grey[600]),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(12, 0, 0, 12),
                        child: Text(
                          "Boarding Pass Description     ",
                          style:
                              TextStyle(fontSize: 14, color: Colors.grey[500]),
                        ),
                        alignment: Alignment.centerLeft,
                      ),
                    ],
                  ),
                  Container(
                    child: Switch(
                      value: val,
                      onChanged: (newVal) {
                        onSwitchValueChanged(newVal);
                      },
                    ),
                  ),
                ],
              ),
            ),
            new Divider(
              color: Colors.grey,
            ),

            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.fromLTRB(12, 8, 10, 3),
                        child: Text(
                          "Client information For Total",
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(12, 0, 0, 12),
                        child: Text(
                          "Boarding Pass Description                 ",
                          style:
                              TextStyle(fontSize: 14, color: Colors.grey[500]),
                        ),
                        alignment: Alignment.centerLeft,
                      ),
                    ],
                  ),
                  Container(
                    child: Switch(
                      value: val,
                      onChanged: (newVal) {
                        onSwitchValueChanged(newVal);
                      },
                    ),
                  ),
                ],
              ),
            ),

            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.fromLTRB(12, 8, 0, 3),
                        child: Text(
                          "Manual Password entry",
                          style:
                              TextStyle(fontSize: 18, color: Colors.grey[600]),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(12, 0, 0, 12),
                        child: Text(
                          "Manual Passport Entry           ",
                          style:
                              TextStyle(fontSize: 14, color: Colors.grey[500]),
                        ),
                        alignment: Alignment.centerLeft,
                      ),
                    ],
                  ),
                  Container(
                    child: Switch(
                      value: val,
                      onChanged: (newVal) {
                        onSwitchValueChanged(newVal);
                      },
                    ),
                  ),
                ],
              ),
            ),
            new Divider(
              color: Colors.grey,
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.fromLTRB(12, 8, 0, 3),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Default Quanitty of Measure',
                      style: TextStyle(fontSize: 18, color: Colors.grey[600]),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 65,
                    padding: const EdgeInsets.fromLTRB(12, 0, 12, 3),
                    child: DropdownButton<String>(
                      isExpanded: true,
                      items: _currencies.map((String dropDownStringItem) {
                        return DropdownMenuItem<String>(
                          value: dropDownStringItem,
                          child: Text(dropDownStringItem),
                        );
                      }).toList(),
                      onChanged: (String newValueSelected) {
                        _dropDownItemSelected(newValueSelected);
                      },
                      value: _currentItemSelected,
                    ),
                  )
                ],
              ),
            ),

            Container(
                padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                child: TextFormField(
                  initialValue: "2",
                  decoration:
                      InputDecoration(labelText: 'Default Decimal Format'),
                )),
            Container(
              margin: const EdgeInsets.only(top: 15),
              padding: const EdgeInsets.only(left: 12),
              child: Text(
                "Other Setting",
                style: TextStyle(
                    fontWeight: FontWeight.w600, color: Colors.grey[700]),
              ),
              color: Colors.grey[300],
              width: double.infinity,
              height: 30.0,
              alignment: Alignment.centerLeft,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.fromLTRB(12, 8, 0, 3),
                        child: Text(
                          "Vibrate                       ",
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(12, 0, 0, 12),
                        child: Text(
                          "Boarding Pass Description",
                          style:
                              TextStyle(fontSize: 14, color: Colors.grey[500]),
                        ),
                        alignment: Alignment.centerLeft,
                      ),
                    ],
                  ),
                  Container(
                    child: Switch(
                      value: val,
                      onChanged: (newVal) {
                        onSwitchValueChanged(newVal);
                      },
                    ),
                  ),
                ],
              ),
            ),

            Container(
              margin: const EdgeInsets.only(top: 15),
              padding: const EdgeInsets.only(left: 12),
              child: Text(
                "Printer Setting",
                style: TextStyle(
                    fontWeight: FontWeight.w600, color: Colors.grey[700]),
              ),
              color: Colors.grey[300],
              width: double.infinity,
              height: 30.0,
              alignment: Alignment.centerLeft,
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(12, 8, 10, 3),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        child: Text(
                          "Printer Mode",
                          style:
                              TextStyle(fontSize: 18, color: Colors.grey[600]),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    child: Text(
                      "Network",
                      style: TextStyle(fontSize: 18, color: Colors.blue[600]),
                    ),
                  ),
                ],
              ),
            ),

            // RaisedButton(
            //   onPressed: () {
            //     // Navigate back to first route when tapped.
            //   },
            //   child: Text('Go back!'),
            // )
          ],
        ),
      ),
    );
  }
}
