import 'dart:core';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:ls_retail/screens/SalesPersonActivity.dart';
import 'package:ls_retail/utils/Constants.dart';
import 'package:ls_retail/utils/ntlmclient.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:ntlm/ntlm.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import "ItemLookUp.dart";
import "shopCollect.dart";

class CartActivity extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return CartState();
  }
}

/// This Widget is the main application widget.
class CartState extends State<CartActivity> {
  static const String _title = '';
  bool isProgressBarShown = false;
  final _scafffoldkey = GlobalKey<ScaffoldState>();
  String receiptNum, receipt;

  NTLMClient client;
  String username, password;

  String result = "Hey there !";

  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        result = qrResult;
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
        });
      } else {
        setState(() {
          result = "Unknown Error $ex";
        });
      }
    } on FormatException {
      setState(() {
        result = "You pressed the back button before scanning anything";
      });
    } catch (ex) {
      setState(() {
        result = "Unknown Error $ex";
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getPrefs().then((val) {
      client = NTLM.initializeNTLM(username, password);
    }).then((val) {
      setState(() {
        receipt = receiptNum;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    final List<int> list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    final Divider mydivider = new Divider(
      color: Colors.grey,
    );
    return MaterialApp(
      title: _title,
      home: Scaffold(
          key: _scafffoldkey,
          appBar: AppBar(
            title: Text(_title),
            backgroundColor: Colors.blue[700],
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.cached),
                onPressed: () {

                },
              ),
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ItemLookUp()),
                  );
                },
              ),
              IconButton(
                icon: Icon(Icons.person_add),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SalesPersonPage()),
                  );
                },
              ),
              IconButton(
                icon: new Icon(FontAwesomeIcons.barcode),
                onPressed: _scanQR,
              )
            ],
          ),
          body: ModalProgressHUD(
              inAsyncCall: isProgressBarShown,
              dismissible: false,
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 5,
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: list.length,
                      itemBuilder: (context, text) {
                        return new Container(
                          margin: EdgeInsets.all(6),
                          width: queryData.size.width,
                          child: Card(
                            elevation: 3,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              // mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Flexible(
                                  flex: 1,
                                  child: Container(
                                    // color: Colors.red,
                                    child: Container(
                                      margin: EdgeInsets.fromLTRB(8, 8, 0, 0),
                                      color: Colors.grey,
                                      width: 50,
                                      height: 50,
                                      child: Text(""),
                                    ),
                                  ),
                                ),
                                Flexible(
                                  flex: 5,
                                  child: Container(
                                      // color: Colors.blue,
                                      child: Row(
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            padding:
                                                EdgeInsets.fromLTRB(5, 8, 0, 5),
                                            child: Text(
                                              "Lorem ipsum",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                padding:
                                                    EdgeInsets.only(left: 8),
                                                child: Text(
                                                  "Qty: 1",
                                                  style:
                                                      TextStyle(fontSize: 13),
                                                ),
                                              ),
                                              Container(
                                                padding:
                                                    EdgeInsets.only(left: 8),
                                                child: Text(
                                                  "Price: 11.70",
                                                  style:
                                                      TextStyle(fontSize: 13),
                                                ),
                                              ),
                                              Container(
                                                padding:
                                                    EdgeInsets.only(left: 8),
                                                child: Text(
                                                  "Disc: 4.68",
                                                  style:
                                                      TextStyle(fontSize: 13),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Padding(
                                            padding:
                                                EdgeInsets.fromLTRB(0, 5, 0, 8),
                                            child: Row(
                                              children: <Widget>[
                                                Container(
                                                  padding:
                                                      EdgeInsets.only(left: 8),
                                                  child: Text(
                                                    "Tax: 00",
                                                    style:
                                                        TextStyle(fontSize: 13),
                                                  ),
                                                ),
                                                Container(
                                                  padding:
                                                      EdgeInsets.only(left: 8),
                                                  child: Text(
                                                    "Sale Person:",
                                                    style:
                                                        TextStyle(fontSize: 13),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Expanded(
                                        child: Container(
                                            //color: Colors.red,
                                            alignment: Alignment.centerRight,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Text(
                                                  "9.02",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            )),
                                      )
                                    ],
                                  )),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                        width: queryData.size.width,
                        height: 120.0,
                        color: Colors.black87,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.fromLTRB(8, 14, 0, 0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(bottom: 8),
                                    child: Text(
                                      "Receipt No: $receipt",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(bottom: 8),
                                    child: Text(
                                      "No of Items : 0",
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                      textAlign: TextAlign.start,
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      "Disc: 0.00 Tax: 0.00",
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                      textAlign: TextAlign.start,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.fromLTRB(0, 10, 8, 0),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(bottom: 8),
                                    child: Text("Cart Mode : Sale",
                                        style: TextStyle(
                                            color: Colors.green, fontSize: 14)),
                                  ),
                                  Text("Total: Rs.200",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold)),
                                  Container(
                                    padding: EdgeInsets.only(top: 10),
                                    child: SizedBox(
                                      height: 28,
                                      child: RaisedButton(
                                        color: Colors.blue[600],
                                        onPressed: () {},
                                        child: Text(
                                          "Payment",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        )),
                  )
                ],
              )),
          drawer: SafeArea(
            child: Drawer(
              child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(14, 18, 0, 12),
                    child: Text(
                      "Transaction",
                      style: TextStyle(
                          color: Colors.grey[700],
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                    ),
                  ),
                  MyDrawerItem(
                    title: "Void Transaction",
                    icon: Icon(
                      FontAwesomeIcons.camera,
                      color: Colors.grey[700],
                      size: 23.0,
                    ),
                    onPressed: () {},
                  ),
                  MyDrawerItem(
                    title: "Total discount percent",
                    icon: Icon(
                      FontAwesomeIcons.camera,
                      color: Colors.grey[700],
                      size: 23.0,
                    ),
                    onPressed: () {},
                  ),
                  MyDrawerItem(
                    title: "Total discount amount",
                    icon: Icon(
                      FontAwesomeIcons.camera,
                      color: Colors.grey[700],
                      size: 23.0,
                    ),
                    onPressed: () {},
                  ),
                  MyDrawerItem(
                    title: "Fixed Discount",
                    icon: Icon(
                      FontAwesomeIcons.camera,
                      color: Colors.grey[700],
                      size: 23.0,
                    ),
                    onPressed: () {},
                  ),
                  MyDrawerItem(
                    title: "Suspend Transaction",
                    icon: Icon(
                      FontAwesomeIcons.cog,
                      color: Colors.grey[700],
                      size: 23.0,
                    ),
                    onPressed: () {},
                  ),
                  mydivider,
                  MyDrawerTitle(title: "Item"),
                  MyDrawerItem(
                    title: "Price Check",
                    icon: Icon(
                      FontAwesomeIcons.share,
                      color: Colors.grey[700],
                      size: 23.0,
                    ),
                    onPressed: () {},
                  ),
                  mydivider,
                  MyDrawerTitle(title: "Shop and Collect"),
                  MyDrawerItem(
                    title: "Shop and Collect",
                    icon: Icon(
                      FontAwesomeIcons.share,
                      color: Colors.grey[700],
                      size: 23.0,
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ShopCollect()),
                      );
                    },
                  ),
                  MyDrawerItem(
                    title: "Update shop and collect",
                    icon: Icon(
                      FontAwesomeIcons.share,
                      color: Colors.grey[700],
                      size: 23.0,
                    ),
                    onPressed: () {},
                  ),
                  mydivider,
                  Container(
                    padding: EdgeInsets.fromLTRB(14, 8, 0, 12),
                    child: Text(
                      "Member Management",
                      style: TextStyle(
                          color: Colors.grey[700],
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                    ),
                  ),
                  MyDrawerItem(
                    title: "Create Member",
                    icon: Icon(
                      FontAwesomeIcons.share,
                      color: Colors.grey[700],
                      size: 23.0,
                    ),
                    onPressed: () {},
                  ),
                  MyDrawerItem(
                    title: "Add Member",
                    icon: Icon(
                      FontAwesomeIcons.share,
                      color: Colors.grey[700],
                      size: 23.0,
                    ),
                    onPressed: () {},
                  ),
                  MyDrawerItem(
                    title: "View Member",
                    icon: Icon(
                      FontAwesomeIcons.share,
                      color: Colors.grey[700],
                      size: 23.0,
                    ),
                    onPressed: () {},
                  ),
                  MyDrawerItem(
                    title: "Shop and Collect",
                    icon: Icon(
                      FontAwesomeIcons.share,
                      color: Colors.grey[700],
                      size: 23.0,
                    ),
                    onPressed: () {},
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }

  Future<void> getPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = await prefs.getString(ConstantString.USERNAME);
    password = await prefs.getString(ConstantString.PASSWORD);
    receiptNum = await prefs.getString(ConstantString.RECEIPT_NUMBER);
    debugPrint("this is receipt number ==> $receiptNum");
  }

  Future<void> displaySnackbar(BuildContext context, msg) {
    final snackBar = SnackBar(
      content: Text('$msg'),
      duration: const Duration(seconds: 2),
    );
    _scafffoldkey.currentState.showSnackBar(snackBar);
  }
}

class MyDrawerTitle extends StatelessWidget {
  //Constructor. { here donte that they are optional, we can only use as new MyCard}

  const MyDrawerTitle({@required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: EdgeInsets.fromLTRB(14, 8, 0, 12),
      child: Text(
        '$title',
        style: TextStyle(
            color: Colors.grey[700], fontWeight: FontWeight.bold, fontSize: 14),
      ),
    );
  }
}

class MyDrawerItem extends StatelessWidget {
  //Constructor. { here donte that they are optional, we can only use as new MyCard}

  const MyDrawerItem(
      {@required this.title, @required this.onPressed, this.icon});

  final String title;
  final Function onPressed;
  final Widget icon;

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      onTap: this.onPressed,
      child: Container(
        child: Row(
          children: <Widget>[
            Container(
                padding: EdgeInsets.fromLTRB(18, 8, 30, 8), child: this.icon),
            Container(
              padding: EdgeInsets.only(top: 8),
              child: Text(
                '$title',
                style: TextStyle(
                    color: Colors.grey[600],
                    //  fontWeight: FontWeight.bold,
                    fontSize: 15),
              ),
            )
          ],
        ),
      ),
    );
  }
}
