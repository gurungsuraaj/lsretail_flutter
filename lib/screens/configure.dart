import 'dart:convert' show Encoding, Utf8Encoder, json, utf8;
import 'package:ls_retail/database/AppDatabase.dart';
import 'package:ls_retail/database/model/UOM.dart';
import 'package:ls_retail/model/PosFunctionalityProfile.dart';
import 'package:ls_retail/model/Store.dart';
import 'package:ls_retail/model/UnitsOfMeasure.dart';
import 'package:ls_retail/utils/Constants.dart';
import 'package:ls_retail/utils/NetworkConnection.dart';
import 'package:ls_retail/utils/NetworkOperationManager.dart';
import 'package:ls_retail/utils/Rcode.dart';
import 'package:ls_retail/utils/Rstring.dart';
import 'package:ls_retail/utils/StringUtils.dart';
import 'package:ls_retail/utils/UrlHelper.dart';
import 'package:ls_retail/utils/ntlmclient.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:ntlm/ntlm.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:xml/xml.dart' as xml;
import 'package:xml2json/xml2json.dart';
import 'GetLicenseKey.dart';

void main() => runApp(Configure());

class Configure extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Configure Screen",
      home: new Scaffold(
        appBar: AppBar(
          title: new Text("Configure Activity"),
        ),
        body: MyConfigurations(),
      ),
    );
  }
}

class MyConfigurations extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyConfigurationState();
  }
}

class _MyConfigurationState extends State<MyConfigurations> {
  bool isProgressBarShown = false;

  Xml2Json xml2json = new Xml2Json();

  var _formKey = GlobalKey<FormState>();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  NTLMClient client;
  NetworkConnectionCheck networkConnectionCheck = new NetworkConnectionCheck();
  int selectedRadio;

  String server = "";
  String service = "";
  String company = "";
  String odataport = "";
  String soapport = "";
  String username = "";
  String password = "";
  String storeid = "";
  String currency = "";
  String terminal = "";
  String selectedRadioText = "";
  Future<bool> internetCheck;


  TextEditingController serverController =
      new TextEditingController(text: "ebtech.dyndns.org");
  TextEditingController serviceController =
      new TextEditingController(text: "ARIPOS2");
  TextEditingController companyController =
      new TextEditingController(text: "ARI POS");
  TextEditingController odataController =
      new TextEditingController(text: "6126");
  TextEditingController soapController =
      new TextEditingController(text: "6125");
  TextEditingController usernameController =
      new TextEditingController(text: "Nipuna");
  TextEditingController passwordController =
      new TextEditingController(text: "Nepal@123");
  TextEditingController storeController =
      new TextEditingController(text: "S1001");
  TextEditingController currencyController = new TextEditingController();
  TextEditingController terminalController =
      new TextEditingController(text: "S1001");

  @override
  void initState() {
    super.initState();

    checkIfNtlmCredentialsExist().then((isExists) {
      if (isExists) {
        getCredentialsFromPrefsAndSetInTextFields();
      }
    });
  }

  setSelectedRadio(int val) {
    setState(() {
      selectedRadio = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("Configuration Manager"),
        ),
        body: ModalProgressHUD(
          inAsyncCall: isProgressBarShown,
          dismissible: false,
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  new Container(
                    margin: EdgeInsets.only(
                        left: 10, top: 20, right: 10, bottom: 10),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: serverController,
                            decoration: InputDecoration(
                              labelText: "Server",
                            ),
                            validator: (value) {
                              if (value.length == 0) {
                                return ('Please enter this field.');
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                        left: 10, top: 0, right: 10, bottom: 10),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: serviceController,
                            decoration: InputDecoration(labelText: "Service"),
                            validator: (value) {
                              if (value.length == 0) {
                                return ('Please enter this field.');
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                        left: 10, top: 0, right: 10, bottom: 10),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: companyController,
                            decoration: InputDecoration(labelText: "Company"),
                            validator: (value) {
                              if (value.length == 0) {
                                return ('Please enter this field.');
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                        left: 10, top: 0, right: 10, bottom: 10),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: odataController,
                            decoration: InputDecoration(
                              labelText: "ODATA Port",
                            ),
                            validator: (value) {
                              if (value.length == 0) {
                                return ('Please enter this field.');
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                        left: 10, top: 0, right: 10, bottom: 10),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: soapController,
                            decoration: InputDecoration(labelText: "SOAP Port"),
                            validator: (value) {
                              if (value.length == 0) {
                                return ('Please enter this field.');
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                        left: 10, top: 0, right: 10, bottom: 10),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: usernameController,
                            decoration: InputDecoration(labelText: "Username"),
                            validator: (value) {
                              if (value.length == 0) {
                                return ('Please enter this field.');
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                        left: 10, top: 0, right: 10, bottom: 10),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: passwordController,
                            decoration: InputDecoration(
                              labelText: "Password",
                            ),
                            validator: (value) {
                              if (value.length == 0) {
                                return ('Please enter this field.');
                              }
                            },
                            obscureText: true,
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                        left: 10, top: 0, right: 10, bottom: 10),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: storeController,
                            decoration: InputDecoration(labelText: 'Store ID'),
                            validator: (value) {
                              if (value.length == 0) {
                                return ('Please enter this field.');
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                        left: 10, top: 0, right: 10, bottom: 10),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: terminalController,
                            decoration:
                                InputDecoration(labelText: 'Terminal ID'),
                            validator: (value) {
                              if (value.length == 0) {
                                return ('Please enter this field.');
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                        left: 10, top: 0, right: 10, bottom: 10),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: currencyController,
                            decoration: InputDecoration(
                              labelText: "Transaction currency symbol",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin:
                        EdgeInsets.only(left: 0, top: 20, right: 0, bottom: 0),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: (new RaisedButton(
                            onPressed: () {
                              networkConnectionCheck.checkInternet(
                                  checkInternetForTestConnection);
                            },
                            textColor: Colors.white,
                            color: Colors.blue,
                            padding: const EdgeInsets.all(8.0),
                            child: new Text(
                              "Test Connection",
                            ),
                          )),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin:
                        EdgeInsets.only(left: 0, top: 20, right: 0, bottom: 0),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          "Note: Please get license Key after testing the connection",
                          style: TextStyle(
                              color: Colors.grey,
                              fontStyle: FontStyle.italic,
                              fontSize: 13.0),
                          textAlign: TextAlign.center,
                        ))
                      ],
                    ),
                  ),
                  new Container(
                    margin:
                        EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 0),
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: (new Divider(
                            color: Colors.black,
                            height: 36,
                          )),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    padding: EdgeInsets.only(top: 0, bottom: 16),
                    child: Column(children: <Widget>[
                      Align(
                          alignment: Alignment.center,
                          child: new GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GetLicense()),
                              );
                            },
                            child: new Text(
                              "Get License Key",
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 16.0,
                                  fontStyle: FontStyle.normal),
                            ),
                          )),
                    ]),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  checkInternetForTestConnection(bool isInternetConnected) {
    //check the status of the form fields and validate
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      //proceed with the process after the form is validated.
      if (isInternetConnected) {
        //first save the credentials into prefs, then proceed with the processes.
        saveNtlmCredentialsToPrefs().then((val) {
          testConnection();
        });
      } else {
        Scaffold.of(context).showSnackBar(new SnackBar(
          content: new Text("No Internet Connection"),
        ));
      }
    }
  }

  void testConnection() async {
    showProgressBar();
    var store_id = storeController.text;
    debugPrint("username ==> $username");

    await NetworkOperationManager.testConnection(store_id, client).then((rs) {
      int status = rs.status;
      var responseBody = rs.responseBody;

      if (responseBody == Rstring.LOGIN_SUCCESS) {
        hideProgressBar();
        displaySnackbar(context, "Login success");
        getCurrencyFromStoreList();
      } else {
        hideProgressBar();
        displaySnackbar(context, "Something went wrong");
      }
    }).catchError((val) {
      hideProgressBar();
      displaySnackbar(context, val);
    });
  }

  void displaySnackbar(BuildContext context, msg) {
    final snackBar = SnackBar(
      content: Text('$msg'),
      duration: const Duration(seconds: 2),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void getCurrencyFromStoreList() async {
    showProgressBar();
    var baseURL = await UrlHelper.getODATAUrl();
    var url =
        baseURL + "/StoreList?\$filter=No eq '" + storeid + "'&\$format=json";
    client.get(url).then((res) {
      var resCode = res.statusCode;

      if (resCode == Rcode.SUCCESS) {
        var data = json.decode(res.body);
        var values = data["value"] as List;
        List<Store> storeList =
            values.map<Store>((json) => Store.fromJson(json)).toList();

        hideProgressBar();
        debugPrint("URL ==> $url");

        if (storeList.length > 0) {
          var functionalityProfile = storeList[0].Functionality_Profile;
          gettingCurrencyFromFunctionalityProfile(functionalityProfile);
        } else {
          displaySnackbar(context, "Something went wrong");
        }
      } else {
        hideProgressBar();
        displaySnackbar(context, "No response from server");
        resetNtlmCredentialsInPrefs();
      }
    }).catchError((val) {
      hideProgressBar();
      displaySnackbar(context, val);
    });
  }

  void gettingCurrencyFromFunctionalityProfile(
      String functionalityProfile) async {
    showProgressBar();
    String encodedFunctionalityProfile =
        StringUtils.encodeHashInString(functionalityProfile);

    var base_url = await UrlHelper.getODATAUrl();
    var url = base_url +
        "/POSFunctionalityProfile?\$filter=Profile_ID eq '" +
        encodedFunctionalityProfile +
        "'&\$format=json";

    client.get(url).then((res) {
      debugPrint("this is url ----------> $url");
      var res_code = res.statusCode;

      if (res_code == Rcode.SUCCESS) {
        hideProgressBar();
        var data = json.decode(res.body);
        var values = data["value"] as List;
        List<PosFunctionalityProfile> functionalityProfileList = values
            .map<PosFunctionalityProfile>(
                (json) => PosFunctionalityProfile.fromJson(json))
            .toList();

        if (functionalityProfileList.length > 0) {
          String currencySymbol =
              functionalityProfileList[0].POS_Currency_Symbol;
          displaySnackbar(context, "The app has been configured.");
          debugPrint("This is currency symbom ==> $currencySymbol");

          setState(() {
            currencyController.text = currencySymbol;
          });

          saveToPrefs(currencySymbol);
          getUnitOfMeasure();
        } else {
          displaySnackbar(context, "Something went wrong");
        }
      } else {
        hideProgressBar();
        displaySnackbar(context, "No response from server");
        resetNtlmCredentialsInPrefs();
      }
    }).catchError((val) {
      hideProgressBar();
      displaySnackbar(context, val);
    });
  }

  void saveToPrefs(String currencySymbol) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(ConstantString.CURRENCY, currencySymbol);
  }

  Future<void> saveNtlmCredentialsToPrefs() async {
    final prefs = await SharedPreferences.getInstance();

    server = serverController.text;
    service = serviceController.text;
    company = companyController.text;
    odataport = odataController.text;
    soapport = soapController.text;
    username = usernameController.text;
    password = passwordController.text;
    storeid = storeController.text;
    currency = currencyController.text;
    terminal = terminalController.text;

    if (selectedRadio == 1) {
      selectedRadioText = "XML";
    } else {
      selectedRadioText = "JSON";
    }

    await prefs.setString(ConstantString.SERVER, server);
    await prefs.setString(ConstantString.SERVICE, service);
    await prefs.setString(ConstantString.COMPANY, company);
    await prefs.setString(ConstantString.ODATA_PORT, odataport);
    await prefs.setString(ConstantString.SOAP_PORT, soapport);
    await prefs.setString(ConstantString.USERNAME, username);
    await prefs.setString(ConstantString.PASSWORD, password);
    await prefs.setString(ConstantString.STORE_ID, storeid);
    await prefs.setString(ConstantString.TERMINAL_ID, terminal);
    await prefs.setString(ConstantString.SELECTED_FORMAT, selectedRadioText);

    client = NTLM.initializeNTLM(username, password);
  }

  Future<bool> checkIfNtlmCredentialsExist() async {
    final prefs = await SharedPreferences.getInstance();

    String ntlmPrefTest = prefs.getString(ConstantString.USERNAME);
    if (ntlmPrefTest == null || ntlmPrefTest.isEmpty) {
      return false;
    }

    return true;
  }

  void getUnitOfMeasure() async {
    showProgressBar();

    var base_url = await UrlHelper.getODATAUrl();
    var url = base_url + "/UnitsofMeasure?\$format=json";

    client.get(url).then((res) async{
      debugPrint("this is url ----------> $url");
      var res_code = res.statusCode;

      if (res_code == Rcode.SUCCESS) {

        hideProgressBar();
        var data = json.decode(res.body);
        var values = data["value"] as List;

        List<UnitsOfMeasure> unitsOfMeasure = values
            .map<UnitsOfMeasure>(
                (json) => UnitsOfMeasure.fromJson(json))
            .toList();

        if (unitsOfMeasure.length > 0) {

          //save units of measures in the database.
          final database = await $FloorAppDatabase.databaseBuilder(ConstantString.db_name).build();

          List<UOM> uomList = new List();
          for( UnitsOfMeasure measure in unitsOfMeasure){
            UOM uom = new UOM(measure.id, measure.code, measure.description);
            uomList.add(uom);
          }

          await database.unitsOfMeasureDao.deleteAll();
          await database.unitsOfMeasureDao.insertAll(uomList);

        } else {
          displaySnackbar(context, "Something went wrong");
        }
      } else {
        hideProgressBar();
        displaySnackbar(context, "No response from server");
      }
    }).catchError((val){
      hideProgressBar();
      displaySnackbar(context, val);
    });
  }


  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }

  Future<void> getCredentialsFromPrefsAndSetInTextFields() async {
    final prefs = await SharedPreferences.getInstance();

    server = await prefs.getString(ConstantString.SERVER);
    service = await prefs.getString(ConstantString.SERVICE);
    company = await prefs.getString(ConstantString.COMPANY);
    odataport = await prefs.getString(ConstantString.ODATA_PORT);
    soapport = await prefs.getString(ConstantString.SOAP_PORT);
    username = await prefs.getString(ConstantString.USERNAME);
    password = await prefs.getString(ConstantString.PASSWORD);
    storeid = await prefs.getString(ConstantString.STORE_ID);
    terminal = await prefs.getString(ConstantString.TERMINAL_ID);
    currency = await prefs.get(ConstantString.CURRENCY);

    setState(() {
      serverController.text = server;
      serviceController.text = service;
      companyController.text = company;
      odataController.text = odataport;
      soapController.text = soapport;
      usernameController.text = username;
      passwordController.text = password;
      storeController.text = storeid;
      currencyController.text = currency;
      terminalController.text = terminal;
    });
  }

  resetNtlmCredentialsInPrefs() async {
    final prefs = await SharedPreferences.getInstance();

    await prefs.setString(ConstantString.SERVER, "");
    await prefs.setString(ConstantString.SERVICE, "");
    await prefs.setString(ConstantString.COMPANY, "");
    await prefs.setString(ConstantString.ODATA_PORT, "");
    await prefs.setString(ConstantString.SOAP_PORT, "");
    await prefs.setString(ConstantString.USERNAME, "");
    await prefs.setString(ConstantString.PASSWORD, "");
    await prefs.setString(ConstantString.STORE_ID, "");
    await prefs.setString(ConstantString.TERMINAL_ID, "");
    await prefs.setString(ConstantString.CURRENCY, "");
    await prefs.setString(ConstantString.SELECTED_FORMAT, "");
  }

}
